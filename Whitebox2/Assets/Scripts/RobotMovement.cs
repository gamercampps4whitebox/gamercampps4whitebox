﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotMovement : MonoBehaviour 
{	
	public float m_speed = 10;
	public float m_rotationSpeed = 10;

	public bool m_isPlayer1 = true;

	Rigidbody m_rigidbody;

	public Animator m_axeAnim;
	public Animator m_flipperAnim;

	void Start()
	{	m_rigidbody = gameObject.GetComponent<Rigidbody> ();
	}

	void Update () 
	{	
		//float x = Input.GetAxis ("Horizontal")  * m_speed;
		//float z = Input.GetAxis ("Vertical")  * m_speed;

		float z = GetYPos () * m_speed;

		m_rigidbody.AddForce (transform.forward*z, ForceMode.Force);

		//float rotation = Input.GetAxis ("Horizontal");

		float rotation = GetRotation ();

		transform.Rotate (new Vector3 (0, rotation, 0) * Time.deltaTime * m_rotationSpeed);

		if ((m_isPlayer1 && Input.GetKeyDown (KeyCode.Q)) ||
		    (!m_isPlayer1 && Input.GetKeyDown (KeyCode.U))) 
		{	m_axeAnim.SetTrigger ("Attack");	
		}

		if ((m_isPlayer1 && Input.GetKeyUp (KeyCode.Q)) ||
		    (!m_isPlayer1 && Input.GetKeyUp (KeyCode.U))) 
		{	m_axeAnim.SetTrigger ("Release");	
		}

		if ((m_isPlayer1 && Input.GetKeyDown (KeyCode.E)) ||
			(!m_isPlayer1 && Input.GetKeyDown (KeyCode.O))) 
		{	m_flipperAnim.SetTrigger ("Attack");	
		}

		if ((m_isPlayer1 && Input.GetKeyUp (KeyCode.E)) ||
			(!m_isPlayer1 && Input.GetKeyUp (KeyCode.O))) 
		{	m_flipperAnim.SetTrigger ("Release");	
		}
	}

	float GetYPos()
	{	
		if (m_isPlayer1) 
		{	
			if (Input.GetKey (KeyCode.S)) 
				return 1;
			else if (Input.GetKey (KeyCode.W))
				return -1;
		} 
		else 
		{	
			if (Input.GetKey (KeyCode.K)) 
				return 1;
			else if (Input.GetKey (KeyCode.I))
				return -1;
		}

		return 0;
	}

	float GetRotation()
	{	
		if (m_isPlayer1) 
		{	
			if (Input.GetKey (KeyCode.D)) 
				return 1;
			else if (Input.GetKey (KeyCode.A))
				return -1;
		} 
		else 
		{	
			if (Input.GetKey (KeyCode.L)) 
				return 1;
			else if (Input.GetKey (KeyCode.J))
				return -1;
		}

		return 0;
	}
}
