﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour 
{
	GameObject[] m_robots;

	float m_timerCounter;
	public float m_speed = 0.1f;

	public float m_radiusOffset = 5;
	public float m_heightOffset = 10;

	public Transform m_cameraDummy;
	public Transform m_cameraDummy2;
	float m_dummyTimerCounter = 0;

	void Update () 
	{	
		m_timerCounter += Time.deltaTime * m_speed;
		m_dummyTimerCounter += Time.deltaTime * 10;

		Vector3 center = GetCenter ();
		float radius = GetRadius (center) + m_radiusOffset;
		
		float x = center.x +  Mathf.Cos (m_timerCounter) * radius;
		float z = center.z + Mathf.Sin (m_timerCounter) * radius;

		transform.position = new Vector3 (x, center.y + m_heightOffset, z);
		//transform.position = Vector3.Lerp (transform.position, new Vector3(x, center.y + m_heightOffset, z), 0.1f);
		transform.LookAt (center);

		float dummyX = center.x + Mathf.Cos (m_dummyTimerCounter) * radius;
		float dummyZ = center.z + Mathf.Sin (m_dummyTimerCounter) * radius;

		m_cameraDummy.transform.position = new Vector3(dummyX, center.y + m_heightOffset, dummyZ);
	
		m_cameraDummy2.transform.position = center;
	}

	Vector3 GetCenter()
	{
		m_robots = GameObject.FindGameObjectsWithTag ("Robot");

		Vector3 pos = Vector3.zero;

		for (int i = 0; i < m_robots.Length; i++) 
		{	pos += m_robots [i].transform.position;
		}

		Vector3 center = pos / m_robots.Length;
		return center;
	}

	float GetRadius(Vector3 center)
	{
		// Note: radius is based on robot farthest away from the center
		float radius = 0;

		for (int i = 0; i < m_robots.Length; i++) 
		{	
			float distance = Vector3.Distance (center, m_robots [i].transform.position);

			if (distance > radius) 
			{	radius = distance;
			}
		}

		return radius;
	}
}
